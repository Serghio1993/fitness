package ru.zaytsev.fitnesproject;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Training.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TrainingDao trainingDao();
}
