package ru.zaytsev.fitnesproject;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class HomeFragment extends Fragment {

    Button btnAddHome;
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppDatabase database = App.getInstance().getDatabase();
        TrainingDao trainingDao = database.trainingDao();

        Training training = new Training();
        training.id = 1;
        training.name = "Жим штанги лёжа";
        trainingDao.insert(training);
        }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home,container,false);
        btnAddHome = view.findViewById(R.id.btnAddHome);
        return view;
    }
}
