package ru.zaytsev.fitnesproject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Training {
    @PrimaryKey
    public int id;
    String name;
}
