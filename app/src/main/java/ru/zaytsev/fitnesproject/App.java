package ru.zaytsev.fitnesproject;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.facebook.stetho.Stetho;

public class App extends Application {

    private AppDatabase db;
    public static App instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        db = Room.databaseBuilder(getApplicationContext(),AppDatabase.class,"database").allowMainThreadQueries().build();
        Stetho.initializeWithDefaults(this);
        Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);

        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );

        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this)
        );

        Stetho.Initializer initializer = initializerBuilder.build();

        Stetho.initialize(initializer);
    }


    public static App getInstance(){
        return instance;
    }
    public AppDatabase getDatabase(){
        return db;
    }
}
