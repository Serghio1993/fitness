package ru.zaytsev.fitnesproject;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentRight extends Fragment {

    Button btnAddRight;
    public static FragmentRight newInstance() {
        FragmentRight fragment = new FragmentRight();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_right,container,false);
        btnAddRight = view.findViewById(R.id.btnAddRight);
        return view;
    }
}
