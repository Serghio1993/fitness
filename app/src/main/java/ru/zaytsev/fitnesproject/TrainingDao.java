package ru.zaytsev.fitnesproject;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

public interface TrainingDao {
    @Query("SELECT * FROM training")
    List<Training>getAll();
    @Query("SELECT * FROM training WHERE id == id")
    Training getById (int id);
    @Insert
    void insert (Training training);
    @Update
    void update (Training training);
    @Delete
    void delete (Training training);
}
