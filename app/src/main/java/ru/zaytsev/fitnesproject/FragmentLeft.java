package ru.zaytsev.fitnesproject;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentLeft extends Fragment {

    Button btnAddLeft;
    public static FragmentLeft newInstance() {
        FragmentLeft fragment = new FragmentLeft();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_left,container,false);
        btnAddLeft = view.findViewById(R.id.btnAddLeft);
        return view;
    }
}
